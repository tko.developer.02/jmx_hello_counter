package Counter;

public class Counter {

    volatile int counter = 0;

    public void increment() {
        this.counter++;
    }

    public void decrement() {
        this.counter--;
    }

    public void incrementBy10() {
        this.counter += 10;
    }

    public void reset() {
        this.counter = 0;
    }

    public void random() {
        this.counter = (int) (Math.random() * 100);
    }

    public int getCount() {
        return this.counter;
    }
}
