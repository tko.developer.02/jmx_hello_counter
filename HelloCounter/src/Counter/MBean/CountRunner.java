package Counter.MBean;

public class CountRunner implements CountRunnerMBean {

    private Counter.Counter counter;

    public CountRunner(Counter.Counter counter) {
        this.counter = counter;
    }

    public void increment() {
        this.counter.increment();
    }

    public void decrement() {
        this.counter.decrement();
    }

    public void incrementBy10() {
        this.counter.incrementBy10();
    }

    public void reset() {
        this.counter.reset();
    }

    public void random() {
        this.counter.random();
    }

    public int getCount() {
        return this.counter.getCount();
    }
}

