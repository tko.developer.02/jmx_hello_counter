package Counter.MBean;

import Counter.Counter;

public interface CountRunnerMBean {

//    void CountRunner(Counter counter);

    void increment();

    void decrement();

    void incrementBy10();

    void reset();

    void random();

    int getCount();
}
