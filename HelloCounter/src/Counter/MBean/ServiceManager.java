package Counter.MBean;

public class ServiceManager implements ServiceManagerMBean {

    private volatile boolean live = true;

    public ServiceManager(){

    }

    public void shutdown() {
        this.live = false;
    }

    public boolean getLiveliness() {
        return this.live;
    }
}

