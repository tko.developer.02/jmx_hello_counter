
import Counter.Counter;
import Counter.MBean.CountRunner;
import Counter.MBean.ServiceManager;

import javax.management.*;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.lang.management.ManagementFactory;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        System.out.println(new Date());

        Counter counter = new Counter();

        System.out.println("Count: " + counter.getCount());

        counter.increment();
        System.out.println("Count: " + counter.getCount());

        counter.incrementBy10();
        System.out.println("Count: " + counter.getCount());

        counter.decrement();
        System.out.println("Count: " + counter.getCount());

        counter.random();
        System.out.println("Count: " + counter.getCount());

        counter.reset();
        System.out.println("Count: " + counter.getCount());

        ServiceManager sm = new ServiceManager();
        String[] mbeanList = {"com.test:name=CountRunner", "com.test:name=ServiceManager"};
        Object[] initObjList = {new CountRunner(counter), sm};

        try {

            for (int i=0; i<mbeanList.length; i++) {

                ObjectName objectName = new ObjectName(mbeanList[i]);
                MBeanServer server = ManagementFactory.getPlatformMBeanServer();
                server.registerMBean(initObjList[i], objectName);
            }

        } catch (Exception e) {
            // handle exceptions
            e.printStackTrace();
        }



        while (sm.getLiveliness()) {

        }
    }
}
