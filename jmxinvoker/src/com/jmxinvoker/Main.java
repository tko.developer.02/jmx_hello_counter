package com.jmxinvoker;

import javax.management.*;
import javax.management.remote.*;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Map<String,Object> env = new HashMap<String,Object>();
        env.put(JMXConnector.CREDENTIALS,new String[]{"admin", "adminpassword"});

        try {
            JMXConnectorFactory.connect(new JMXServiceURL(args[0]), env)
                    .getMBeanServerConnection().invoke(new ObjectName(args[1]), args[2], new Object[]{}, new String[]{});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
